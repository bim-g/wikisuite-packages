#!/bin/bash

PHP_VERSIONS="5.6 7.1 7.2 7.3 7.4 8.0 8.1 8.2"

PHP_VERSIONS_TO_INSTALL="7.4 8.1 8.2"

# path to wikisuite-virtualmin-all
WS_VIRTUALMIN_ALL_PATH="$(dirname "$PWD")/wikisuite-virtualmin-all/"

# cleanup files
for FILE in debian/control debian/postinst debian/*.postinst debian/prerm debian/*.prerm
do
  if [ -f "${FILE}" ]
  then
    echo "Removing ${FILE}"
    rm "${FILE}"
  fi
done

# generate debian files
echo "Adding source package to control"
cat partials/control.part.source > debian/control

for VERSION in ${PHP_VERSIONS}
do
  echo "Adding binary package to control for PHP ${VERSION}"
  cat partials/control.part.binary | sed "s/%VERSION%/${VERSION}/g" >> debian/control
  echo "Adding install file list for PHP ${VERSION}"
  cat partials/install | sed "s/%VERSION%/${VERSION}/g" > debian/wikisuite-php${VERSION}.install
  echo "Adding postinst script for PHP ${VERSION}"
  cat partials/postinst | sed "s/%VERSION%/${VERSION}/g" > debian/wikisuite-php${VERSION}.postinst
  chmod 755 debian/wikisuite-php${VERSION}.postinst
  echo "Adding prerm script for PHP ${VERSION}"
  cat partials/prerm | sed "s/%VERSION%/${VERSION}/g" > debian/wikisuite-php${VERSION}.prerm
  chmod 755 debian/wikisuite-php${VERSION}.prerm
done

# get dependencies in wikisuite-virtualmin-all control file
DEPENDS="$(sed -n -e '/^Depends:/,/.*:/ {/^Depends:/p; /.*:/!p}' ${WS_VIRTUALMIN_ALL_PATH}debian/control | sed 's/^Depends://; s/.*://')"

for VERSION in ${PHP_VERSIONS_TO_INSTALL}
do
  if [ -f ${WS_VIRTUALMIN_ALL_PATH}debian/control ]; then
    if ! echo "$DEPENDS" | grep -oq "wikisuite-php${VERSION}"; then
      echo "Adding dependency for PHP ${VERSION} to wikisuite-virtualmin-all control file"
      last_line=$(echo "$DEPENDS" | awk 'END{print}')
      sed -i "/${last_line}/ s/$/, wikisuite-php${VERSION}/" ${WS_VIRTUALMIN_ALL_PATH}debian/control
    fi
  else
    echo "Unable to update wikisuite-virtualmin-all control file, path ${WS_VIRTUALMIN_ALL_PATH}debian/control does not exist."
  fi
done